/** @type {import('tailwindcss').Config} */
export default {
  content: [
    "./index.html",
    "./src/**/*.{vue,js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      fontFamily : {
        raleWay: ["Raleway", "sans-serif"]
      }
    },
  },
  plugins: [
    require('tailwindcss-animated')
  ],
}